import axios from 'axios'
import instance from "./connection"
class PredictionCropsApi{
    
    cropsData(data){
        console.log("comming data",data)
        return axios({
            url:instance()+'predict_crop',
            method:'POST',
            data:data
        })

    }
}
export class VisualizeData{
    getChartData(){
        return axios({
            url:instance()+'graph_visualize',
            method:'GET'
        })
    }
}
export default PredictionCropsApi