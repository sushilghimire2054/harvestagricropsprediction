import React, { useState, useEffect } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

export const AlertNotification = (props)=> {
    const [alertState,setAlertState] = useState({})
    let open = false
    let messageRender = ""
    let severity = "success"

  useEffect(()=>{
    if (props.openState != null){
      setAlertState(props.openState)
    }
  })
    const handleSuccessClose =() =>{
        props.onChange(false)
    }
  
    
    return (
        <Snackbar open={alertState.open} autoHideDuration={5000} onClose={handleSuccessClose}>

            <Alert onClose={handleSuccessClose} severity={alertState.severity}>
            {alertState.message}
        </Alert>
      </Snackbar>
    )
}

export const TestAlertNotification = ({open=false,message="",severity=""}) =>{
  useEffect(()=>{
    setAlertState(alertState=>alertState={...alertState,open:open,message:message,severity:severity})
  },[])
  const [alertState,setAlertState] = useState({
    open:null,
    message:null,
    severity:null
  })

  const handleSuccessClose = () =>{
    setAlertState(alertState=>alertState={...alertState,open:false})
  }
  return (
    <Snackbar open={alertState.open} autoHideDuration={5000} onClose={handleSuccessClose}>

        <Alert onClose={handleSuccessClose} severity={alertState.severity}>
        {alertState.message}
    </Alert>
  </Snackbar>
)
}