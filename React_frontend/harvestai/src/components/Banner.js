import { Button, Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import React from 'react';
import '../Styles/Banner.css'
import {useNavigate}  from "react-router-dom";

const Banner = () => {
    const useStyles=makeStyles({
        drawPaper:{background:'#FFFACD'},
       
    })
    const history=useNavigate();
    const cropRedirect=()=>{
        history('/crop');
    }
    const visualizeChart=()=>{
        history('/visualize_data'); 
    }
  const classes=useStyles();
    return (
        <div className="banner">
            <div className="banner__title">
                <div className="banner__title_head">
                    HARVEST<font><em>I</em>FY</font>
                </div>
                <div className="banner__title_tail">
                    <div className="typing">HARVESTIFY</div>
                    <div className="typing">A Machine Learning based Web Application for Crop Recommendation</div>
                    <p className="typography">Get informed Decisions About Farming Strategy.</p>
                    <div className="banner__buttons">
                        <Button onClick={cropRedirect} className="banner__button cropButton">Crop Recommender</Button>
                        <Button onClick={visualizeChart} className="banner__button cropButton">Data Visualize</Button>
                    </div>

                    {/* <div className="banner__socialMedia">
                        <a className="social_icon_linkedin" href="https://www.linkedin.com/in/sushil-ghimire-20546/" target="_blank"><span ><i className="fa fa-linkedin" aria-hidden="true"></i></span></a>
                        <a className="social_icon_gitlab" href="https://gitlab.com/sushilghimire2054/harvestagricropsprediction" target="_blank"><span><i className="fa fa-gitlab" aria-hidden="true"></i></span></a>
                    </div> */}


                    <div>
                        <br></br>
                        <Paper elevation={2} className={classes.drawPaper}>
                            <Grid item xs={10}>
                                <Typography>
                                    <u>Note:</u> This M.L application is for educational/demo purposes only and cannot be relied upon.
                        Check the source code <a href='https://gitlab.com/sushilghimire2054/harvestagricropsprediction'>here</a>
                                </Typography>

                            </Grid>
                        </Paper>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Banner
