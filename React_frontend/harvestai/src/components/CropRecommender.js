import React, { useState } from 'react'
import { useForm } from 'react-hook-form';
import { TextField, Button, Grid, CircularProgress,createMuiTheme } from "@material-ui/core";
import '../Styles/croprecommender.css'
import { makeStyles } from "@material-ui/core/styles";

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import PredictionCropsApi from '../services/predict_crops';
import Alert from '@material-ui/lab/Alert';
import { cropData } from "./CropsData/Data";
import '../Styles/croprecommenderoutput.css';
import { AlertNotification } from './Alert';
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";

const cropsPredict = new PredictionCropsApi()

const useStyles = makeStyles({
    root: {
        maxWidth: 550,
    },
    table: {
        minWidth: 450,
    },
})
// for asterisk
const formLabelsTheme = createMuiTheme({
    overrides: {
      MuiFormLabel: {
        asterisk: {
          color: "#db3131",
          "&$error": {
            color: "#db3131"
          }
        }
      }
    }
  });      
const CropRecommender = () => {
    const { register, formState: { errors }, handleSubmit,reset } = useForm({
        mode: 'onChange'
    });

    const [formData, setFormData] = useState({
        N: "",
        P: "",
        K: "",
        temperature: "",
        humidity: "",
        ph: "",
        rainfall: ""
    })
    console.log("formdata", formData)
    const [predictionData, setPredictionData] = useState({

    })
    console.log("predictionData", predictionData)
    const [loadingStatus, setLoadingStatus] = useState(false)
    const [error, setError] = useState(false)
    const [errorText,setErrorText]=useState('');
    const [alertCondition, setAlertCondition] = useState({
        open: false,
        message: "",
        severity: ""
    })
    const handleAlertClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        let resetAlert = {
            open: false,
            message: "",
            severity: ""
        }

        setAlertCondition(resetAlert)
    };
    const handleChange = (e) => {
        const validate=e.target.value;
        console.log(validate)
        if(isNaN(validate) || validate>299)
        {
            setErrorText("String is not valid")
        }
        else{
            let newData = { ...formData }
            newData[e.target.name] = e.target.value
            setFormData(newData)
            setErrorText('')
        }   

    }
    const handleChangePh=(e)=>{
        const validate=e.target.value;
        console.log(validate)
        if(isNaN(validate) || validate>16)
        {
            setErrorText("String is not valid")
        }
        else{
            let newData = { ...formData }
            newData[e.target.name] = e.target.value
            setFormData(newData)
            setErrorText('')
        }

    }

    const handleClick = (data) => {
        console.log("clicking data", data)
        setLoadingStatus(true)
        const requestObject = new FormData()

        for (let key in formData) {
            requestObject.append(key, formData[key])
        }
        // let json=JSON.stringify(requestObject);        
        console.log("requestObbject", requestObject)
        cropsPredict.cropsData(requestObject).then((res) => {
            console.log("Successful predict");                         
            setAlertCondition({
                message: "Successfully predicted!",
                open: true,
                severity: "success"

            })
            setPredictionData(res.data);
            setLoadingStatus(false);   
            
            

        }).catch(error => {
            setLoadingStatus(false);
            console.log("error occured")
            setAlertCondition({
                message: "Invalid Error occured!",
                open: true,
                severity: "error"
            })
            setError(true)
            console.log("cannot fetch", error)
        })
    }
    const handleBackClick = () => {
        setPredictionData({})
    }

    const classes = useStyles();

    const predictedCrop = cropData[predictionData.rf_model_prediction];
    // console.log("prediction crops recommendations",predictedCrop)

    if (predictionData.rf_model_prediction) {
        const outputComponent = (

            <div className="output_container">
                <Card className={`${classes.root} output_container__card`}>
                    {/* <CardActionArea> */}
                    <CardMedia
                        component="img"
                        alt={predictedCrop.title}
                        height="225"
                        image={predictedCrop.imageUrl}
                        title={predictedCrop.title}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            <b>Prediction: </b>{predictedCrop.title}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {predictedCrop.description}
                        </Typography>
                        <br />
                        <Paper elevation={3}>
                            <TableContainer>
                                <Table className={classes.table} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell component="th" align="center"><b>RandomForest Model Prediction</b></TableCell>

                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow>
                                            <TableCell align="center">{predictionData.rf_model_prediction} ({predictionData.rf_model_probability}%)</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Paper>

                    </CardContent>
                    {/* </CardActionArea> */}
                    <CardActions>
                        <Button onClick={() => handleBackClick()} className="back__button" variant="contained" size="small" color="primary">
                            Back to Prediction
                        </Button>
                        <AlertNotification onChange={handleAlertClose} openState={alertCondition} />
                    </CardActions>
                </Card>
            </div>

        )
        return outputComponent
    }
    if (loadingStatus) {
        return <CircularProgress color="info" size={90} style={{ marginLeft: "50%", marginTop:"15%", color: 'white' }} />
    }

    else return (
        <div className="form">

            <div className="form__form_group">
                {/* {

                    error &&
                    <Alert style={{ marginTop: "20px" }} severity="error">Please enter valid data</Alert>
                } */}
                <AlertNotification onChange={handleAlertClose} openState={alertCondition} />
                <MuiThemeProvider theme={formLabelsTheme}>
                <form onSubmit={handleSubmit(handleClick)}>
                    <Paper elevation={3}>
                        <Grid container direction="row" spacing={2}>
                            <center>

                                <div className="form__title" style={{ marginLeft: "10px", width: '93%' }}>Find out the most suitable crop to grow in your farm</div>
                            </center>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    value={formData.N}
                                    onChange={handleChange}
                                    className="form__text_field"
                                    style={{ width: '97%' }}
                                    name="N"
                                    variant="outlined"
                                    label="Amount of Nitrogen in Soil(0-140)"
                                    error={errorText}

                                />

                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    value={formData.P}
                                    onChange={handleChange}
                                    className="form__text_field"
                                    style={{ width: '97%' }}
                                    name="P"
                                    variant="outlined"
                                    label="Amount of Phosphorous in Soil(5-145)"
                                    error={errorText}

                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    value={formData.K}
                                    onChange={handleChange}
                                    className="form__text_field"
                                    style={{ width: '97%' }}
                                    name="K"
                                    variant="outlined"
                                    label="Amount of Pottasium in Soil(5-205)"
                                    error={errorText}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required    
                                    value={formData.temperature}
                                    onChange={handleChange}
                                    className="form__text_field"
                                    style={{ width: '97%' }}
                                    name="temperature"
                                    variant="outlined"
                                    label="Temperature (in Celcius)(9-44)"
                                    error={errorText}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    value={formData.humidity}
                                    onChange={handleChange}
                                    className="form__text_field"
                                    style={{ width: '97%' }}
                                    name="humidity"
                                    variant="outlined"
                                    label="Humidity (in %)(14-100)"
                                    error={errorText}

                                />
                            </Grid>
                            <Grid item xs={12}>

                                <TextField
                                    required
                                    value={formData.ph} 
                                    onChange={handleChangePh}
                                    className="form__text_field"
                                    style={{ width: '97%' }}
                                    name="ph"
                                    variant="outlined"
                                    label="pH value of Soil(3-8)"
                                    error={errorText}

                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    value={formData.rainfall}
                                    onChange={handleChange}
                                    className="form__text_field"
                                    style={{ width: '97%' }}
                                    name="rainfall"
                                    variant="outlined"
                                    label="Rainfall (in mm)(22-299)"
                                    error={errorText}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <center>
                                    <Button className="form__button" onClick={(e)=>handleClick(e)} color="primary" variant="contained" style={{ width: '97%' }}
                                    >
                                        Predict Crop
                                        {/* {loadingStatus ? <CircularProgress color="inherit" size={20} /> : "Predict Crop"} */}
                                    </Button>
                                </center>
                            </Grid>
                        </Grid>
                    </Paper>
                </form>
                </MuiThemeProvider>
            </div>

        </div>
    )
}

export default CropRecommender
