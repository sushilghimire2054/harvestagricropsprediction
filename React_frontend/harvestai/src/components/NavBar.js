import React, { useState, useEffect } from 'react'
import { useNavigate, NavLink } from 'react-router-dom';
import { MenuList } from './MenuList';
import '../Styles/NavBar.css'
const NavBar = () => {
    const [clicked, setClicked] = useState(false);

    const history = useNavigate();
    const menuList = MenuList.map(({ url, title }, index) => {
        return (
            <li key={index}>
                <NavLink to={url} activeClassName="active">
                    {title}
                </NavLink>
            </li>
        )

    })

    const handleTitleClick = () => {
        history('/');
    }
    const handleClick = () => {
        setClicked(!clicked);
    }
    const closeMobileMenu = () => {
        setClicked(false);
    }
    return (
        <nav>
            <div className="logo" onClick={handleTitleClick}>
                HARVEST<font><em>I</em>FY</font>
            </div>
            <div className="menu-icon" onClick={handleClick}>
                <i className={clicked ? "fa fa-times" : "fa fa-bars"}></i>
            </div>
            <ul className={clicked ? "menu-list" : "menu-list close"} onClick={closeMobileMenu}>{menuList}</ul>
        </nav>

    )
}

export default NavBar
