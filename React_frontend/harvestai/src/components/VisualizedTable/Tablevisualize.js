import { Card, CardContent, CircularProgress, Paper, TextField, Typography } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { VisualizeData } from '../../services/predict_crops';
import DynamicTable from './DynamicTable';
const dataVisualize = new VisualizeData()

const useStyle=makeStyles((theme)=>({
    paper: {
        padding: theme.spacing(2),
    }
}))
const Tablevisualize = () => {
    const classes=useStyle()
    const [showTable, setShowTable] = useState([]);
    const [loading, setLoading] = useState(false);
    // table search
    useEffect(() => {
        getTableData();
    }, [])
    const getTableData = () => {
        setLoading(true)
        dataVisualize.getChartData().then((res) => {
            if (res && res.data) {
                console.log("data visualize response:", res.data)
                setShowTable(res.data)
                setLoading(false)
            }

        }).catch(err => {
            setLoading(false)
            console.log("cannot find data", err)
        })

    }
    const columnData=[
        {title:'Nitrogen (N)',field:'N'},
        {title:'Phosphrous (P)',field:'P'},
        {title:'Potassium (K)',field:'K'},
        {title:'temp (celcius)',field:'temperature'},
        {title:'humidity (%)',field:'humidity'},
        {title:'pH',field:'ph'},
        {title:'rainfall (mm)',field:'rainfall'},
        {title:'Crops',field:'label'},

    ]
    return (
        <Paper elevation={0} className={classes.paper}>
        <Card varient='outlined' container direction="row" item xs={8}>
            <CardContent>
                <Typography varient='h6'>
                    <b>Bivarient Table Analysis</b>
                </Typography>
                {loading ? <CircularProgress color="inherit" size={40} style={{marginLeft:'50%'}}/> : <DynamicTable loading={loading} data={showTable} column={columnData} paginationDisplay={true}/> }
               
            </CardContent>
        </Card>
        </Paper>
    )
};

export default Tablevisualize;
