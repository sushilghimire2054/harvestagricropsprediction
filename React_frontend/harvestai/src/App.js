import "./App.css";
import Banner from "./components/Banner";
import NavBar from "./components/NavBar";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'//in version 6 , switch is replaced by Routes  
import Home from './components/Home';
import CropRecommender from "./components/CropRecommender";
import Tablevisualize from "./components/VisualizedTable/Tablevisualize";

function App() {
  return (
    <div style={{
      backgroundImage: `url(${process.env.PUBLIC_URL + 'assets/background.jpg'})`
    }} className="container">
      <div className="overlay">
        <Router>
          <NavBar />
          <Routes>
            <Route exact path="/visualize_data" element={<Tablevisualize/>}></Route>
            <Route exact path="/" element={<Home/>}/>
            <Route exact path="/crop" element={<CropRecommender/>}/>
          </Routes>
        </Router>
      </div>
    </div>
  );
}

export default App;
