from flask import Flask,request,json, Response
import time
# import json
from scipy import stats
import pickle
import csv

import numpy as np
app=Flask(__name__)
data='./Data-processed/Crop_recommendation.csv'
# # loading crops Recommendation Models

crop_rf_pipeline=pickle.load(
    open("./models/crop_recommendation/RandomForestPipeline.pkl","rb")
)

def convert(o):
    if isinstance(o, np.generic):
        return o.item()
    raise TypeError

def crop_prediction(input_data):
    prediction_data={
        "rf_model_prediction":crop_rf_pipeline.predict(input_data),
        "rf_model_probability": max(crop_rf_pipeline.predict_proba(input_data))*100
    }
    
    all_predictions= prediction_data['rf_model_prediction']
    print("prediction",all_predictions)
    
    all_probs=prediction_data["rf_model_probability"]
    print("total probability",all_probs)
    maxn = max(all_probs)
    print('props:',maxn)
    final_data = {}
    if len(set(all_predictions))==len(all_predictions):
        # prediction_data["final_prediction"]=all_predictions[all_probs.index(max(all_probs))]
        print("collections of unique size",len(all_predictions))
        final_data['rf_model_prediction'] = all_predictions[0]
        final_data['rf_model_probability'] = maxn
    else:
        prediction_data["final_predition"]=stats.mode(all_predictions)
    print("final prediction",prediction_data)
    
    return final_data

@app.route("/predict_crop", methods=["POST"])
def predictcrop():
    payload = {
        'status': None,
        'message': None, 'details': []
    }
    try:
        if request.method == "POST":
            print('in post')
            print('susu',request.form.to_dict())
            form_values = request.form.to_dict()
            column_names = ["N", "P", "K", "temperature", "humidity", "ph", "rainfall"]
            arr = np.asarray([[float(form_values[i].strip())] for i in column_names])
            # print("array before reshape",arr)
            input_data = arr.reshape(1, -1)
            # print("array after reshape",input_data)
            prediction_data = crop_prediction(input_data)
            json_obj = json.dumps(prediction_data,default=convert)
            payload['status'] = 200
            payload['message'] = "Prediction is successful"
            payload['details'] = json_obj
            # print("payloads",payload)
            # return  Response(payload, mimetype="application/json")
            time.sleep(1)
            return json_obj
    except:
        payload['message'] = "Please Enter Valid Data"
        return Response(payload, status=400, mimetype="application/json")
# convert CSV file to json
def csv_to_json(csvFilePath):
    jsonArray = []
      
    #read csv file
    with open(csvFilePath, encoding='utf-8') as csvf: 
        #load csv file data using csv library's dictionary reader
        csvReader = csv.DictReader(csvf) 

        #convert each csv row into python dict
        for row in csvReader: 
            #add this python dict to json array
            row['label'] = str(row["label"])
            
            jsonArray.append(row)

    #convert python jsonArray to JSON String and write to file
    jsonString = json.dumps(jsonArray, indent=4, )
    return jsonString
          
csvFilePath = r'./Data-processed/Crop_recommendation.csv'
# for data visualization
@app.route('/graph_visualize',methods=['GET'])
def getChartData():

    file = csv_to_json(csvFilePath)
    # return file
    print(file)
    return Response(file, status=200, mimetype="application/json")
        
if __name__=='main':
    app.run(debug=True,host="0.0.0.0")
